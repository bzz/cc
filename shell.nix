#
#  Development Environment Sandbox
#

with import <nixpkgs> {};

let
  sandbox-exec = stdenv.mkDerivation {
    name = "sandbox-exec";
    src = /usr/bin/sandbox-exec;
    unpackPhase = "true";
    buildPhase = "true";
    installPhase = "mkdir -p $out/bin && ln -s $src $out/bin/sandbox-exec";
  };
in

stdenv.mkDerivation {
  name = "cc";
  src = if lib.inNixShell then null else ./.;

  shellHook = ''
    eval `opam env -y`
    # TODO: reuse parent shell variables
    export TERM="''${TERM:-linux}"
    export OPAMSOLVERTIMEOUT=120
  '';

  buildInputs = with ocaml-ng.ocamlPackages_4_08; [
    opam
    ocaml
    dune
    utop
    odoc
    merlin
    ocp-indent
    emacs
    gitMinimal
    colordiff
    gmp
    m4
    perl
    which
    zeromq
  ] ++ stdenv.lib.optional stdenv.isDarwin sandbox-exec;


}
