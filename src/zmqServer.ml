(**

   There are 5 socket spots


   for a random peer:
   check if already connected
   if we have unconnected spot
   we connect to the peer through a DEALER socket on that spot, send HELLO.

   - We read messages from our ROUTER socket, and each message comes with the UUID of the sender.
     If it's an HELLO message, we connect back to that peer if not already connected to it.
     If it's any other message, we must already be connected to the peer (a good place for an assertion).
     If we receive a new peer: 
     save the peer;
     if we have unconnected spot
     we connect to the peer through a DEALER socket on that spot, send HELLO.

   - We send messages to each peer using the per-peer DEALER socket, which must be connected.
   - When we connect to a peer, we also tell our application that the peer exists.
   - Every time we get a message from a peer, we treat that as a heartbeat (it's alive).

*)

(* let peer1_sock : [`Dealer] Zmq.Socket.t ref =
 *   ref (Zmq.Socket.create Settings.zmq_context Zmq.Socket.dealer) *)

let handle sock =
  let%lwt sl = Zmq_lwt.Socket.recv_all sock in
  match sl with
  | [uuid; packet] ->
      let _ = Lwt_log.info_f "GOT UUID [%s]" (Z.to_string (Z.of_bits uuid)) in
      let _ = Lwt_log.info_f "GOT PACKET [%s]" packet in
      let _ =
        match Packet.detect packet with
        | `Hello ->
            (* ( try
         *     let endpoint = (Packet.Hello.unpack packet).endpoint in
         *     if Database.Connections.socket_index_for uuid = -1 then
         *       let i = Database.Connections.socket_index_for "" in
         *       if i = -1 then Lwt_log.info "CONNECTION LIMIT"
         *       else
         *         let _ = Lwt_log.info_f "socket now on index %d" i in
         *         let x = !(Database.Connections.array.(i)) in
         *         let _ = Zmq.Socket.connect x endpoint in
         *         let _ = Database.Connections.array.(i) := x in
         *         let so = Zmq_lwt.Socket.of_socket x in
         *         let%lwt _ = Zmq_lwt.Socket.send so packet in
         *         Lwt_log.info "NOW CONNECT BACK"
         *     else Lwt_log.info "ALREADY CONNECTED"
         *   with e ->
         *     let _ = Lwt_log.info (Printexc.to_string e) in
         *     Lwt_log.info "BAD ENDPOINT" ) *)
            Lwt_log.info "GOT HELLO"
        | `PeerOnline ->
            Lwt_log.info "GOT PEER ONLINE"
        (*         let _ = Event.got_online_peer (Packet.PeerOnline.unpack s).peer in
       * handler_loop conn *)
        | _ ->
            Lwt_log.info "GOT UNRECOGNIZED PACKET"
      in
      Lwt.return ()
  | _ ->
      Lwt.return ()

let rec loop sock =
  let%lwt _ = handle sock in
  loop sock

let inquire_version sockref peer =
  let endpoint = "tcp://" ^ peer in
  let _ = Lwt_log.info_f "inquire_version %s" endpoint in
  let _ = Zmq.Socket.connect !sockref endpoint in
  let v =
    let addrhost =
      String.concat ""
        [ "tcp://"
        ; Unix.string_of_inet_addr Unix.inet_addr_any
        ; ":"
        ; string_of_int !Settings.zmq_port ]
    in
    Packet.Hello.pack {endpoint = addrhost; peers = []}
    (* {version = Settings.version; peers = Peer.Database.list_online ()} *)
  in
  let so = Zmq_lwt.Socket.of_socket !sockref in
  let f () =
    (* let xx = Zmq.Socket.get_last_endpoint !sockref in
     * Printf.printf "%s %!" xx ;    
     * Zmq.Socket.disconnect !sockref xx ; *)
    Zmq.Socket.close !sockref ;
    sockref := Zmq.Socket.create Settings.zmq_context Zmq.Socket.dealer ;
    Printf.printf "Timeout.\n%!"
  in
  let timeout = Lwt_timeout.create Settings.zmq_timeout_secs f in
  let _ = Lwt_timeout.start timeout in
  let%lwt _ = Zmq_lwt.Socket.send so v in
  Lwt.return ()

let init_zmq_server () =
  let sock = Zmq.Socket.create Settings.zmq_context Zmq.Socket.router in
  let rec attempt_init_server sock port =
    try
      let endpoint = "tcp://*:" ^ Printf.sprintf "%d" port in
      Zmq.Socket.bind sock endpoint ;
      Settings.zmq_port := port ;
      Lwt.return ()
    with e -> attempt_init_server sock (port + 1)
  in
  let%lwt _ = attempt_init_server sock !Settings.zmq_port in
  let _ = Lwt_log.info_f "ZMQ server started on port %d" !Settings.zmq_port in
  let _ = Database.load (Settings.peers_filename ()) in
  let s1 = Database.Connections.array.(0) in
  (* Zmq.Socket.create Settings.zmq_context Zmq.Socket.dealer in *)

  (* let test_lst =
   *   let rec f n acc =
   *     match n with
   *     | 0 ->
   *       acc
   *     | _ ->
   *       f (n - 1) (("127.0.0.1:" ^ string_of_int (2000 + n)) :: acc)
   *   in
   *   f 1000 []
   * in *)
  (* let lst = Database.Peers.elements_randomized () in *)
  (* let _ = Lwt_list.map_p (fun peer -> inquire_version !s1 peer) lst in *)
  let%lwt _ =
    let head = List.hd (Database.Peers.elements_randomized ()) in
    let head2 = List.hd @@ List.tl (Database.Peers.elements_randomized ()) in
    let%lwt _ = inquire_version s1 head in
    inquire_version s1 head2
  in
  try loop (Zmq_lwt.Socket.of_socket sock)
  with e -> Lwt_log.info (Printexc.to_string e)
