(* constants *)
let version = 1
let worker_heartbeat_secs = 0.5
let worker_save_timeout_secs = 5.
let max_json_length = 10000
let zmq_context = Zmq.Context.create ()
let zmq_timeout_secs = 3

(* variables *)
let rpc_port = ref 45592
let zmq_port = ref 2001
let peers_filename () = Printf.sprintf "peersdb_%i.txt" !zmq_port

let peers_fd_out =
  try ref @@ open_out_gen [Open_creat] 0 (peers_filename ())
  with _ -> ref stdout
