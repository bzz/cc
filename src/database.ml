module StringSet = Set.Make (String)

module Peers = struct
  let data : StringSet.t ref = ref StringSet.empty
  let add s = data := StringSet.add s !data
  let get s = StringSet.find_opt s !data
  let mem s = StringSet.mem s !data
  let elements () = StringSet.elements !data

  let elements_randomized () =
    let shuffle d =
      let nd = List.map (fun c -> (Random.bits (), c)) d in
      let sond = List.sort compare nd in
      List.map snd sond
    in
    shuffle (elements ())
end

let save_exn oc =
  let data = String.concat "\n" (StringSet.elements !Peers.data) in
  output_string oc data

let load filename =
  let ic = open_in_gen [Open_creat] 0 filename in
  let read_lines ic =
    let read_line i = try Some (input_line i) with End_of_file -> None in
    let rec go i acc =
      match read_line i with
      | None ->
          close_in ic ;
          List.rev acc
      | Some s when String.length s > 0 ->
          go i (s :: acc)
      | _ ->
          go i acc
    in
    go ic []
  in
  let lines = read_lines ic in
  List.map Peers.add lines

(* not to be saved *)

module Connections = struct
  type t = [`Dealer] Zmq.Socket.t ref

  let count = 5

  let array : t array =
    Array.make count
      (ref (Zmq.Socket.create Settings.zmq_context Zmq.Socket.dealer))

  let socket_index_for uuid =
    let rec go n =
      if Zmq.Socket.get_identity !(array.(n)) = uuid then n
      else if n + 1 < count then go (n + 1)
      else -1
    in
    go 0
end
