let saved_last = ref (Unix.gettimeofday ())

(* let node_started = Unix.gettimeofday ()
 * let show_status () =
 *   let%lwt _ =
 *     Lwt_log.info_f "\n----(Seconds working: %f)----"
 *       (Unix.gettimeofday () -. node_started)
 *   in
 *   let lp = List.map Peer.to_string (Peer.Database.list ()) in
 *   let%lwt _ = Lwt_log.info_f "----Peers: \n[%s]" (String.concat "]\n[" lp) in
 *   let cl =
 *     List.map HostKey.Key.to_string (Connection.Database.list_inbound ())
 *   in
 *   let%lwt _ =
 *     Lwt_log.info_f "----Inbound connections: \n   %s"
 *       (String.concat "\n   " cl)
 *   in
 *   let ll = List.map Connection.to_string (Connection.Database.list ()) in
 *   let%lwt _ =
 *     Lwt_log.info_f "...Connected to: \n%s\n----------" (String.concat "\n" ll)
 *   in
 *   Lwt.return () *)

let save_all () =
  if Unix.gettimeofday () > !saved_last +. Settings.worker_save_timeout_secs
  then
    let _ = Database.save_exn !Settings.peers_fd_out in
    let _ = saved_last := Unix.gettimeofday () in
    (* let%lwt _ = show_status () in *)
    Lwt.return ()
  else Lwt.return ()

let rec loop () =
  let%lwt _ = Lwt_unix.sleep Settings.worker_heartbeat_secs in
  (* let _ = save_all () in *)
  loop ()

let start () =
  Lwt_log.add_rule "*" Lwt_log.Info ;
  Lwt.async (fun () -> RpcServer.init_rpc_server ()) ;
  Lwt.async (fun () -> ZmqServer.init_zmq_server ()) ;
  Lwt.async (fun () -> loop ()) ;
  let forever, _ = Lwt.wait () in
  Lwt_main.run forever

(**
   (* dealer or req ? *)

   let ctx = Zmq.Context.create ();;
   let socket = Zmq.Socket.create ctx Zmq.Socket.req;;
   Zmq.Socket.connect socket "tcp://127.0.0.1:2001";;

   let msg =
    let open Bigarray in
    let data = Array1.create char c_layout 1000000 in
    Array1.fill data '\x00' ;
    Array1.set data 0 't' ;
    Array1.set data 1 'e' ;
    Array1.set data 2 's' ;
    Array1.set data 3 't' ;
    Zmq.Msg.init_data data

   Zmq.Socket.send_msg socket msg;;
*)
