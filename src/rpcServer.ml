open Httpaf

module JSON = struct
  open Yojson.Basic
  open Yojson.Basic.Util

  let createMessage ~from ~to_ ~body =
    `Assoc [("from", `String from); ("to", `String to_); ("body", `String body)]

  let responseMessage json =
    let from = json |> member "from" |> to_string in
    let to_ = json |> member "to" |> to_string in
    let body = json |> member "body" |> to_string in
    Yojson.Basic.to_string
    @@ `Assoc [("server_got_message", createMessage ~from ~to_ ~body)]
end

let is_json_post_request reqd =
  match Reqd.request reqd with
  | {Request.meth = `POST; headers; _} ->
    ( match Headers.get headers "content-type" with
    | Some "application/json" ->
        true
    | Some _ ->
        false
    | None ->
        false )
  | _ ->
      false

let parse_json_request body_str : Bigstringaf.t =
  try
    let json = Yojson.Basic.from_string (Bigstringaf.to_string body_str) in
    let resp = JSON.responseMessage json in
    Bigstringaf.of_string ~off:0 ~len:(String.length resp) resp
  with _ ->
    let s = "Invalid JSON: " ^ Bigstringaf.to_string body_str in
    Bigstringaf.of_string ~off:0 ~len:(String.length s) s

let request_handler reqd =
  if is_json_post_request reqd then
    let request_body : [`read] Httpaf.Body.t = Reqd.request_body reqd in
    let to_buf = Bigstringaf.create Settings.max_json_length in
    let rec on_read buffer ~off ~len =
      Bigstringaf.blit buffer to_buf ~src_off:0 ~dst_off:off ~len ;
      (* to_buf := Bigstring.append !to_buf buffer ; *)
      Body.schedule_read request_body ~on_eof ~on_read
    and on_eof () =
      let response =
        let headers =
          Headers.of_list
            [("content-type", "application/json"); ("connection", "close")]
        in
        Response.create ~headers `OK
      in
      Reqd.respond_with_bigstring reqd response (parse_json_request to_buf)
    in
    Body.schedule_read request_body ~on_eof ~on_read
  else
    let headers = Headers.of_list [("connection", "close")] in
    Reqd.respond_with_string reqd
      (Response.create ~headers `Bad_request)
      "Bad request"

let error_handler ?request:_ error start_response =
  let response_body = start_response Headers.empty in
  ( match error with
  | `Exn exn ->
      Body.write_string response_body (Printexc.to_string exn) ;
      Body.write_string response_body "\n"
  | #Status.standard as error ->
      Body.write_string response_body (Status.default_reason_phrase error) ) ;
  Body.close_writer response_body

let init_rpc_server () =
  let rec attempt_init_rpc port =
    try
      let listen_address = Unix.(ADDR_INET (inet_addr_loopback, port)) in
      let request_handler (_ : Unix.sockaddr) = request_handler in
      let error_handler (_ : Unix.sockaddr) = error_handler in
      let%lwt _ =
        Lwt_io.establish_server_with_client_socket listen_address
          (Httpaf_lwt.Server.create_connection_handler ~request_handler
             ~error_handler)
      in
      let%lwt _ = Lwt_log.info_f "JSON RPC started on port %d..." port in
      Lwt.return ()
    with e ->
      (* TODO Unix.Unix_error(Unix.EADDRINUSE, "bind", "")*)
      attempt_init_rpc (port + 1)
  in
  let%lwt _ = attempt_init_rpc !Settings.rpc_port in
  Lwt.return ()
