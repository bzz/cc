type packet =
  [ `InvalidPacket
  | `Hello
  | `PeerOnline ]

type t = packet

let t_arr : packet array = [|`InvalidPacket; `Hello; `PeerOnline|]

let index_of x =
  let rec go x n = if t_arr.(n) = x then n else go x (n + 1) in
  try go x 1 with _ -> 0

let enc_arr a = CBOR.Simple.encode (`Array a)

let dec_arr a =
  match CBOR.Simple.decode a with `Array x -> x | _ -> [] | exception _ -> []

let detect a =
  match dec_arr a with
  | [] ->
      `InvalidPacket
  | x :: xs ->
    (match x with `Int x -> t_arr.(x) | _ -> `InvalidPacket)

let value t ~default = match t with None -> default | Some x -> x

module Hello = struct
  let t : packet = `Hello

  type data =
    { endpoint : string
    ; peers : string list }

  let unpack_opt a =
    let decode a =
      let rec go = function
        | [] ->
            []
        | `Text a :: rest ->
            a :: go rest
        | _ :: rest ->
            go rest
      in
      match a with
      | `Text endpoint :: peers ->
          Some {endpoint; peers = go peers}
      | _ ->
          None
    in
    match dec_arr a with
    | `Int n :: rest when n = index_of t ->
        decode rest
    | _ ->
        None

  let unpack a = value (unpack_opt a) ~default:{endpoint = ""; peers = []}

  let pack {endpoint; peers} =
    let encode l =
      let f a = `Text a in
      List.map f l
    in
    enc_arr @@ [`Int (index_of t)] @ [`Text endpoint] @ encode peers
end

module PeerOnline = struct
  let t : packet = `PeerOnline

  type data = {peer : string}

  let unpack_opt a =
    match dec_arr a with
    | [`Int n; `Text peer] when n = index_of t ->
        Some {peer}
    | _ ->
        None

  let unpack a = value (unpack_opt a) ~default:{peer = ""}

  let pack peer =
    let encode l = [`Text l] in
    enc_arr @@ [`Int (index_of t)] @ encode peer
end

(** utop # 

    open Shh;;
    let l = Packet.Version.pack 
{version=1; peers=["peer1"; "peer2"]};;
    Packet.detect l;;
    Packet.Version.unpack_opt l;;
    let k = Packet.PeerOnline.pack "aaa";;
    Packet.detect k;;
    Packet.PeerOnline.unpack_opt k;;

*)
