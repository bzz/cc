opam-version: "2.0"
name: "CC"
synopsis: "CC"

description: """
CC
"""

license: "GPLv2"
maintainer: "m.baynov@gmail.com"
authors: [
  "Mikhail Bainov <m.baynov@gmail.com>"
]
homepage: "https://bitbucket.org/bzz/cc"
bug-reports: "https://bitbucket.org/bzz/cc/issues"
dev-repo: "git+https://bitbucket.org/bzz/cc"

available: os = "linux" | os = "macos"

build: [
  ["opam" "config" "exec" "--" "dune" "subst"] {pinned}
  ["opam" "config" "exec" "--" "dune" "build" "-p" name "-j" jobs]
]
install: ["opam" "config" "exec" "--" "dune" "install"]

run-test: ["dune" "runtest" "-p" name]

depends: [
  "core"
  "zmq"
  "zmq-lwt"
  "conf-zmq"
  "angstrom"
  "bigstringaf"
  "faraday"
  "faraday-lwt-unix"
  "yojson"
  "nocrypto"
  "zarith"
  "lwt"
  "lwt_ppx"
  "cbor"
  "dune" {build}
  "alcotest" {with-test}
  "odoc" {with-doc & >= "1.1.1"}
  "ocaml" {= "4.06.1"}
  "opam-lock" {dev}
  "tuareg" {dev}
  "merlin" {dev}
  "utop" {dev}
  "ocamlformat" {dev}
  "ocp-indent" {dev}
  "ocp-index" {dev}
  "ocp-browser" {dev}
]
